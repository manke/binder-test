[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fmanke%2Fbinder-test/HEAD)
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/thomasmanke/binder-test/blob/main/Notebooks/MC_000.ipynb)

# Binder Test

Testing binder from gitlab
